package task4;

import java.io.*;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

public class App {

    private String lines = "";
    private List<String> list = new ArrayList<>();

    public void read() throws IOException {
        String line;
        try (Scanner input = new Scanner(System.in)) {

            while (!(line = input.nextLine()).isEmpty()) {
                list.add(line);
            }
        }
    }

    private List<String> getWordsList() {
        return list.stream().flatMap(e -> Stream.of(e.split(" "))).collect(Collectors.toList());
    }

    public long countWords() {
        return getWordsList().stream().distinct().count();
    }

    public List<String> sortListOfUniqueWords() {
        return getWordsList().stream().distinct().sorted().collect(Collectors.toList());
    }

    public Map<String, Long> wordOccurenceNumber() {
        return getWordsList().stream().collect(groupingBy(Function.identity(), counting()));
    }

    public Map<Character, Long> countEachCharNumber() {
        return getWordsList().toString().chars()
                .mapToObj(c -> (char) c)
                .filter(Character::isLowerCase)
                .collect(groupingBy(Function.identity(), counting()));
    }

    public static void main(String[] args) {
        System.out.println("Enter text:");
        App a = new App();
        try {
            a.read();
        } catch (IOException e) {
            System.out.println(e);
        }
        System.out.println(a.countWords());
        a.sortListOfUniqueWords().stream().forEach(System.out::println);
        a.wordOccurenceNumber().forEach((k, v) -> System.out.println(String.format("%s - %d", k, v)));
        a.countEachCharNumber().forEach((k, v) -> System.out.println(String.format("%c->%d", k, v)));
    }
}
