package task1;

@FunctionalInterface
interface Operation {
    int accept(int a, int b, int c);
}
class Main{
    public static void main(String[] args) {
        Operation max = (a, b, c) -> {return (a>b)?(a>c)?a:c:(b>c)?b:c;};
        Operation average = (a, b,c ) ->{return (a+b+c)/3;};

        System.out.println(max.accept(10,4,2));
        System.out.println(average.accept(10,4,2));
    }
}