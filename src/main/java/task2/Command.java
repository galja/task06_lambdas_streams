package task2;

interface Command {
    void execute(String argument);
}
