package task2;

public class Receiver {

    public void countNumberOfCharacters(String string) {
        System.out.println("The string contains "+string.length()+" characters");;
    }

    public void findTheShortestWord(String string) {
        String [] word = string.split(" ");
        String minWord = "";
        for(int i = 0; i < word.length; i++){
            if(word[i].length() <= minWord.length()){
                minWord = word[i];
            }
        }
        System.out.println(minWord);
    }

    public void findTheLongestWord(String string) {
        String [] word = string.split(" ");
        String maxWord = "";
        for(int i = 0; i < word.length; i++){
            if(word[i].length() >= maxWord.length()){
                maxWord = word[i];
            }
        }
        System.out.println(maxWord);
    }

    public void print(String string) {
        System.out.println(string + ": " );
    }
}
