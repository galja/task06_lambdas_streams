package task2;

public class FindTheLongestWord implements Command {
    private Receiver receiver;

    public FindTheLongestWord() {
        receiver = new Receiver();
    }

    @Override
    public void execute(String argument) {
        receiver.findTheLongestWord(argument);
    }
}
