package task2;

import java.util.InputMismatchException;
import java.util.Scanner;
import org.apache.logging.log4j.Logger;


public class Invoker {

    private Command command;
    private Scanner in;
    private Receiver receiver;
    private String value;
    //private static Logger logger = Logger.getLogger(Invoker.class);
    public Invoker() {
        in = new Scanner(System.in);
        receiver = new Receiver();
    }

    public void makeChoice() {
        showMenu();
        System.out.println("Enter string: ");
        this.value = in.nextLine();
        int cmd=0;
        System.out.println("Enter command:");
        try {
            cmd = in.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Wrong input");
        }
        excecuteCommand(cmd);
    }

    private void excecuteCommand(int cmd) {
        if (cmd == 1) {
            command = receiver::countNumberOfCharacters;
            command.execute(value);
        } else if (cmd == 2) {
            command = new Command() {
                @Override
                public void execute(String argument) {
                    System.out.println(argument.replaceAll("\\s+", ""));
                }
            };
            command.execute(value);
        } else if (cmd == 3) {
            command = s -> receiver.findTheShortestWord(value);
        }
    }

    private void scanString(){
        System.out.println("Enter string: ");
        this.value = in.nextLine();
        //return value;
    }
    private void showMenu() {
        System.out.println("1 - count number of characters");
        System.out.println("2 - delete spaces");
        System.out.println("3 - find the longest word");
        System.out.println("4 - find the shortest word");
    }
}
