package task3;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class RandomIntegerList {
    List<Integer> fillListWithRandomNumbers(int limit, int low, int high) {
        return new Random().ints(limit, low, high).boxed().collect(Collectors.toList());
    }

    List<Integer> getListWithRandomNumbers(int limit, int low, int high) {
        return new Random().ints(low, high).distinct().limit(limit).boxed().collect(Collectors.toList());
    }

    int[] getRandomStreamArray(int limit, int low, int high) {
        return new Random().ints(limit, low, high).toArray();
    }

    List<Integer> getRandomArray(int limit) {
        return Stream.generate(() -> ThreadLocalRandom.current().nextInt()).limit(limit).collect(Collectors.toList());
    }

    int getMaxValue(List<Integer> list) {
        return list.stream().mapToInt(v -> v).max().orElseThrow(NoSuchElementException::new);
    }

    int getMinValue(List<Integer> list) {
        return list.stream().mapToInt(v -> v).min().orElseThrow(NoSuchElementException::new);
    }

    double getAverage(List<Integer> list) {
        return list.stream().mapToInt(v -> v).average().orElseThrow(NoSuchElementException::new);
    }

    int getSumOfArray() {
        int[] arr = getRandomStreamArray(10, 0, 19);
        return Arrays.stream(arr).sum();
    }

    int getSumOfList(List<Integer> list) {
        return list.stream().mapToInt(v -> v).reduce(Integer::sum).getAsInt();
    }

    long biggerThanAverageValuesNumber(List<Integer> list) {
        return list.stream().filter(v -> v > getAverage(list)).count();
    }
}
