package task3;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        RandomIntegerList ril = new RandomIntegerList();
        List<Integer> list = ril.fillListWithRandomNumbers(10, 10, 30);
        list.stream().forEach(System.out::print);
        System.out.println("\nMax value: " + ril.getMaxValue(list));
        System.out.println("Min value: " + ril.getMinValue(list));
        System.out.println("Sum: " + ril.getSumOfList(list));
        System.out.print("Average: " + ril.getAverage(list));
        long t = ril.biggerThanAverageValuesNumber(list);
        System.out.println("Number of values bigger than average: " + t);
    }
}
